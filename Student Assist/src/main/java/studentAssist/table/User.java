package studentAssist.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User {

    @Id @GeneratedValue
    private long id;
    private String name;
    private String password;
    private int points;
//
//    @OneToMany
//    private Set<UserSubject> userSubjectSet = new HashSet<>();

    public User(){

    }


    public User(String name, String password, int points) {
        this.name = name;
        this.password = password;
        this.points = points;
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
        points = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

//    public Set<UserSubject> getUserSubjectSet() {
//        return userSubjectSet;
//    }
//
//    public void setUserSubjectSet(Set<UserSubject> userSubjectSet) {
//        this.userSubjectSet = userSubjectSet;
//    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", points=" + points +

                '}';
    }
}
