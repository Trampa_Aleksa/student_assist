package studentAssist.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Subject {

    @Id @GeneratedValue
    private long id;
    private String name;
    private int semester;


//    @OneToMany
//    private Set<UserSubject> userSubjectSet = new HashSet<>();

    public Subject(String name, int semester) {
        this.name = name;
        this.semester = semester;
    }

    public Subject() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

//    public Set<UserSubject> getUserSubjectSet() {
//        return userSubjectSet;
//    }
//
//    public void setUserSubjectSet(Set<UserSubject> userSubjectSet) {
//        this.userSubjectSet = userSubjectSet;
//    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", semester=" + semester +
                '}';
    }
}
