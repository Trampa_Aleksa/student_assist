package studentAssist.table;

import javax.persistence.*;

@Entity
public class Module {

    @Id @GeneratedValue
    private long id;
    private String name;
    private int weekNumber;
    private int colloquiumNumber;
    private int difficulty;
    @ManyToOne
    private Subject subject;

    public Module() {
    }

    public Module(String name, int weekNumber, int colloquiumNumber, int difficulty, Subject subject) {
        this.name = name;
        this.weekNumber = weekNumber;
        this.colloquiumNumber = colloquiumNumber;
        this.difficulty = difficulty;
        this.subject = subject;
    }

    public Module(String name, int weekNumber, int difficulty) {
        this.name = name;
        this.weekNumber = weekNumber;
        this.difficulty = difficulty;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getColloquiumNumber() {
        return colloquiumNumber;
    }

    public void setColloquiumNumber(int colloquiumNumber) {
        this.colloquiumNumber = colloquiumNumber;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Module{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weekNumber=" + weekNumber +
                ", colloquiumNumber=" + colloquiumNumber +
                ", difficulty=" + difficulty +
                '}';
    }
}
