package studentAssist.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class SubjectModule {

    @Id  @GeneratedValue
    private long id;
    @ManyToOne
    private Subject subject;
    @ManyToOne
    private Module module;

    public SubjectModule(Subject subject, Module module) {
        this.subject = subject;
        this.module = module;
    }

    public SubjectModule() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    @Override
    public String toString() {
        return "SubjectModule{" +
                "id=" + id +
                ", subject=" + subject +
                ", module=" + module +
                '}';
    }
}
