package studentAssist.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.transaction.Transactional;

@Entity
public class UserSubject {
    @Id @GeneratedValue
    private long id;

    @ManyToOne
    private User user;
    @ManyToOne
    private Subject subject;

    public UserSubject(User user, Subject subject) {
        this.user = user;
        this.subject = subject;
    }

    public UserSubject() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "UserSubject{" +
                "id=" + id +
                ", user=" + user +
                ", subject=" + subject +
                '}';
    }
}
