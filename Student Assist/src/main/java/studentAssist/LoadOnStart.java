package studentAssist;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import studentAssist.repository.RepositoryStorage;
import studentAssist.table.*;

import java.util.Optional;

@Component
@Order(2)
public class LoadOnStart implements CommandLineRunner {


    private RepositoryStorage repositoryStorage;

    public LoadOnStart(RepositoryStorage repositoryStorage){
        this.repositoryStorage = repositoryStorage;
   }

    @Override
    public void run(String... args) throws Exception {
        //initial repository data
        User user = new User("acc1", "password1");
        Subject subject1 = new Subject("Programiranje 1",2);
        Subject subject2 = new Subject("Programiranje 2", 3);
        UserSubject userSubject;
        repositoryStorage.getUserRepository().save(user);
        repositoryStorage.getUserRepository().save(new User("acc2", "password2"));
        repositoryStorage.getSubjectRepository().save(subject1);
        repositoryStorage.getSubjectRepository().save(subject2);
        repositoryStorage.getSubjectRepository().save(new Subject("Marketing" , 3));
        repositoryStorage.getSubjectRepository().save(new Subject("Arhitekture Racunara i Operativni Sistemi" , 3));
        repositoryStorage.getSubjectRepository().save(new Subject("Racunarske Mreze i Tehnologije" , 5));
        repositoryStorage.getSubjectRepository().save(new Subject("Operaciona Istrazivanja 1" , 5));
        userSubject = new UserSubject(user, subject1);
        repositoryStorage.getUserSubjectRepository().save(userSubject);
        repositoryStorage.getUserSubjectRepository().save(new UserSubject(user, subject2));
        Module module1 = repositoryStorage.getModuleRepository().save(new Module("Modul 1", 1,1 ,1,subject1));
        Module module2 = repositoryStorage.getModuleRepository().save(new Module("Modul 2", 2,1 ,4,subject1));
        repositoryStorage.getModuleRepository().save(new Module("Modul 3", 3,1 ,2,subject1));
        repositoryStorage.getModuleRepository().save(new Module("Modul 4", 4,1 ,3,subject1));
        repositoryStorage.getSubjectModuleRepository().save(new SubjectModule(subject1,module1));
        repositoryStorage.getSubjectModuleRepository().save(new SubjectModule(subject1,module2));

    }
}
