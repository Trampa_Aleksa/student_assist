package studentAssist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import studentAssist.table.UserSubject;

public interface UserSubjectRepository extends JpaRepository<UserSubject, Long> {
}
