package studentAssist.repository;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class RepositoryStorage {

    private ModuleRepository moduleRepository;
    private UserRepository userRepository;
    private SubjectRepository subjectRepository;
    private  UserSubjectRepository userSubjectRepository;
    private  SubjectModuleRepository subjectModuleRepository;



    private RepositoryStorage(ModuleRepository moduleRepository,
                             UserRepository userRepository,
                             SubjectRepository subjectRepository,
                             UserSubjectRepository userSubjectRepository,
                             SubjectModuleRepository subjectModuleRepository) {
        this.moduleRepository = moduleRepository;
        this.userRepository = userRepository;
        this.subjectRepository = subjectRepository;
        this.userSubjectRepository = userSubjectRepository;
        this.subjectModuleRepository = subjectModuleRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public SubjectRepository getSubjectRepository() {
        return subjectRepository;
    }

    public UserSubjectRepository getUserSubjectRepository() {
        return userSubjectRepository;
    }

    public ModuleRepository getModuleRepository() {
        return moduleRepository;
    }

    public SubjectModuleRepository getSubjectModuleRepository() {
        return subjectModuleRepository;
    }
}
