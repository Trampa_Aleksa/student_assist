package studentAssist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import studentAssist.repository.custom.UserRepositoryCustom;
import studentAssist.table.User;

public interface UserRepository extends JpaRepository<User, Long> , UserRepositoryCustom {
}
