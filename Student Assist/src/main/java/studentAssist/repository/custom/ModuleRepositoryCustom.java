package studentAssist.repository.custom;

import studentAssist.table.Module;
import studentAssist.table.Subject;

import java.util.List;

public interface ModuleRepositoryCustom  {

     List<Module> findModulesWithColloquiumNumber(int colloquiumNumber);
     List<Module> getSortedModulesListByAscendingDifficulty();
     List<Module> getSortedModulesListByDescendingDifficulty();
     Module findModuleByName(String name);
     List<Module> findAllModulesOfASubject(long subjectId);
}
