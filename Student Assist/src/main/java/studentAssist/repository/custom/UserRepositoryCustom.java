package studentAssist.repository.custom;

import studentAssist.table.Subject;
import studentAssist.table.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;

public interface UserRepositoryCustom {

    List<User> getUsersWithName(String name);
    List<Subject> getSubjectsOfUser(User user);


}
