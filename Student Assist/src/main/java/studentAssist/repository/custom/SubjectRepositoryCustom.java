package studentAssist.repository.custom;

import studentAssist.table.Subject;
import studentAssist.table.User;

import java.util.List;

public interface SubjectRepositoryCustom {

    Subject findSubjectByName(String name);
    List<Subject> findSubjectsWithSemester(int semester);
    List<User> findUsersOfSubject(Subject subject);

}
