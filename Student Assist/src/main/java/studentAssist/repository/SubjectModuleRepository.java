package studentAssist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import studentAssist.table.SubjectModule;

public interface SubjectModuleRepository extends JpaRepository<SubjectModule,Long> {
}
