package studentAssist.repository.implementation;

import studentAssist.repository.custom.UserRepositoryCustom;
import studentAssist.table.Subject;
import studentAssist.table.User;
import studentAssist.table.UserSubject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UserRepositoryImpl implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<User> getUsersWithName(String name){

//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<User> q = cb.createQuery(User.class);
//        Root<User> c = q.from(User.class);
//        q.select(c);
//        ParameterExpression<Integer> p = cb.parameter(Integer.class);
//        TypedQuery<User> query = entityManager.createQuery(q);
//        query.setParameter(p, name);
//        List<Country> results = query.getResultList();
//        q.where(cb.gt(c.get(name), p));
        TypedQuery<User> query = entityManager.createQuery(
                "SELECT u FROM User u WHERE u.name = :name", User.class);
        return query.setParameter("name", name).getResultList();
    }

    @Override
    public List<Subject> getSubjectsOfUser(User user) {
        TypedQuery<UserSubject> query = entityManager.createQuery(
                "SELECT u FROM UserSubject u WHERE u.user = :user", UserSubject.class);

         List<Subject> subjectsOfUser = new ArrayList<>();
         //get the userSubjects with the passed user and extract a list of subjects from those users
         for(UserSubject currentUserSubject : query.setParameter("user", user).getResultList()){
             subjectsOfUser.add(currentUserSubject.getSubject());
         }
         return subjectsOfUser;
    }
}
