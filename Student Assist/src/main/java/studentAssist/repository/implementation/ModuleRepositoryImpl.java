package studentAssist.repository.implementation;

import studentAssist.repository.custom.ModuleRepositoryCustom;
import studentAssist.table.Module;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ModuleRepositoryImpl implements ModuleRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Module findModuleByName(String name){
        TypedQuery<Module> query = entityManager.createQuery(
                "Select m FROM Module m WHERE m.name = :name", Module.class);
        return query.setParameter("name",name).getSingleResult();
    }

    @Override
    public List<Module> findAllModulesOfASubject(long subjectId) {

        TypedQuery<Module> query = entityManager.createQuery(
                "SELECT m FROM Module m WHERE m.subject.id = :subjectId",Module.class);

        return query.setParameter("subjectId", subjectId).getResultList();
    }

    @Override
    public List<Module> findModulesWithColloquiumNumber(int colloquiumNumber) {
        TypedQuery<Module> query = entityManager.createQuery(
                "SELECT m FROM Module m WHERE m.colloquiumNumber = :colloquiumNumber", Module.class);
        return query.setParameter("colloquiumNumber", colloquiumNumber).getResultList();
    }

    @Override
    public List<Module> getSortedModulesListByAscendingDifficulty() {
        TypedQuery<Module> query = entityManager.createQuery(
                "SELECT m FROM Module m ORDER BY m.difficulty ASC", Module.class);
        return query.getResultList();
    }

    @Override
    public List<Module> getSortedModulesListByDescendingDifficulty() {
        TypedQuery<Module> query = entityManager.createQuery(
                "SELECT m FROM Module m ORDER BY m.difficulty DESC", Module.class);
        return query.getResultList();
    }


}
