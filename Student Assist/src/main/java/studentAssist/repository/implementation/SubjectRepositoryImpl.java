package studentAssist.repository.implementation;

import studentAssist.repository.custom.SubjectRepositoryCustom;
import studentAssist.table.Subject;
import studentAssist.table.User;
import studentAssist.table.UserSubject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class SubjectRepositoryImpl implements SubjectRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Subject findSubjectByName(String name) {
        TypedQuery<Subject> query = entityManager.createQuery(
                "SELECT s FROM Subject s WHERE s.name = :name", Subject.class);
        return query.setParameter("name", name).getSingleResult();
    }

    @Override
    public List<Subject> findSubjectsWithSemester(int semester) {
        TypedQuery<Subject> query = entityManager.createQuery(
                "SELECT s FROM Subject s WHERE s.semester = :semester", Subject.class);
        return query.setParameter("semester", semester).getResultList();
    }

    //TODO -- Almost duplicate code for find Subjects of User in UserResource, refactor a bit
    @Override
    public List<User> findUsersOfSubject(Subject subject) {
        TypedQuery<UserSubject> query = entityManager.createQuery(
                "SELECT u FROM UserSubject u WHERE u.subject = :subject", UserSubject.class);

        List<User> usersOfSubject = new ArrayList<>();
        //get the userSubjects with the passed user and extract a list of subjects from those users
        for(UserSubject currentUserSubject : query.setParameter("subject", subject).getResultList()){
            usersOfSubject.add(currentUserSubject.getUser());
        }
        return usersOfSubject;
    }
}
