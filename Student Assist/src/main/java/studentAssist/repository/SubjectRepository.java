package studentAssist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import studentAssist.repository.custom.SubjectRepositoryCustom;
import studentAssist.table.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> , SubjectRepositoryCustom {
}
