package studentAssist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import studentAssist.repository.custom.ModuleRepositoryCustom;
import studentAssist.table.Module;

public interface ModuleRepository extends JpaRepository<Module, Long>, ModuleRepositoryCustom {
}
