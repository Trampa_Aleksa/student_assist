package studentAssist.rest.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import studentAssist.repository.RepositoryStorage;
import studentAssist.table.Module;

import javax.persistence.EntityNotFoundException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class ModuleResource {

    private static final int maximumNumberOfModules = 28;
    private static final int maximumModuleDifficulty = 5;

    private final
    RepositoryStorage repositoryStorage;

    @Autowired
    public ModuleResource(RepositoryStorage repositoryStorage) {
        this.repositoryStorage = repositoryStorage;
    }

    @GetMapping("/modules")
    public ResponseEntity<?>  getAllModules(){
        List<Module> modules = repositoryStorage.getModuleRepository().findAll();

        return !modules.isEmpty()? new ResponseEntity<>(modules, HttpStatus.OK)
                : new ResponseEntity<>("No modules are present", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/modules/id")
    public Module  getModuleById(@RequestParam long id){
        Optional<Module> module = repositoryStorage.getModuleRepository().findById(id);

        if(module.isPresent()) return module.get();
        else{
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Module Not Found", new EntityNotFoundException());
        }

    }

    @GetMapping("modules/name")
    public Module getModuleByName(@RequestParam String name){

        Module module = repositoryStorage.getModuleRepository().findModuleByName(name);
        if(module!=null){
            return module;
        }
        else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Module Not Found", new EntityNotFoundException());
        }
    }

    @GetMapping("modules/subject")
    public List<Module> getAllModulesOfSubject(@RequestParam long id){

        List<Module> modules = repositoryStorage.getModuleRepository().findAllModulesOfASubject(id);

        if(!modules.isEmpty()){
            return modules;
        }
        else{
            //TODO -- separate "no subject with that id found and the subject doesn't have any modules;
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No subject with that id found, or the subject doesn't have any modules",new EntityNotFoundException());
        }
    }

    @GetMapping("modules/colloquiumNumber")
    public ResponseEntity<?> getModulesForColloquium(@RequestParam int colloquiumNumber){

        List<Module> modules = repositoryStorage.getModuleRepository().findModulesWithColloquiumNumber(colloquiumNumber);

        return !modules.isEmpty()? new ResponseEntity<>(modules,HttpStatus.OK):
                new ResponseEntity<>("No module found",HttpStatus.NOT_FOUND);

    }

    //TODO -- refactor to merge ascending and descending request
    @GetMapping("modules/difficulty/ascending")
    public ResponseEntity<?> getSortedModulesByAscendingDifficulty(){
        List<Module> modules = repositoryStorage.getModuleRepository().getSortedModulesListByAscendingDifficulty();

        return !modules.isEmpty()? new ResponseEntity<>(modules, HttpStatus.OK)
                : new ResponseEntity<>("No modules present", HttpStatus.NOT_FOUND);
    }

    @GetMapping("modules/difficulty/descending")
    public ResponseEntity<?> getSortedModulesByDescendingDifficulty(){
        List<Module> modules = repositoryStorage.getModuleRepository().getSortedModulesListByDescendingDifficulty();

        return !modules.isEmpty()? new ResponseEntity<>(modules, HttpStatus.OK)
                : new ResponseEntity<>("No modules present", HttpStatus.NOT_FOUND);
    }

    @PostMapping("/modules/post")
    public ResponseEntity<?> addModule(@RequestBody Module module, @RequestParam String subjectName){
        module.setSubject(repositoryStorage.getSubjectRepository().findSubjectByName(subjectName));
        ResponseEntity<?> moduleIsOkayResponse = moduleIsOkay(module);


        //TODO -- null checks inside of entity?
       if(moduleIsOkayResponse.getStatusCode() == HttpStatus.OK) {
           Module savedModule = repositoryStorage.getModuleRepository().save(module);
        System.out.println(savedModule.getWeekNumber());
           URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                   .buildAndExpand(savedModule.getId()).toUri();

           return ResponseEntity.created(location).build();
       }
        return moduleIsOkayResponse;
    }

    /**
     * Checks all the properties of the module to see if they are null or out of bounds
     * @param module the module to check
     * @return Ok status response or Partial content response with co-responding message if something was wrong
     */
    private ResponseEntity<?> moduleIsOkay(Module module){
        if(module.getName() != null) {
            if (module.getWeekNumber() > 0 || module.getWeekNumber() <= maximumNumberOfModules) {
                if (module.getDifficulty() > 0 || module.getDifficulty() <= maximumModuleDifficulty) {
                    if (module.getSubject() != null) {
                            return ResponseEntity.ok(module);
                    }
                    else return new ResponseEntity<>("Module has no subject in it", HttpStatus.PARTIAL_CONTENT);
                }
                else return new ResponseEntity<>("Difficulty must be between 0 and " + maximumModuleDifficulty, HttpStatus.PARTIAL_CONTENT);
            }
            else return new ResponseEntity<>("Number of Modules must be between 0 and "+ maximumNumberOfModules, HttpStatus.PARTIAL_CONTENT);
        }
        else return new ResponseEntity<>("Name is null", HttpStatus.PARTIAL_CONTENT);
    }

}
