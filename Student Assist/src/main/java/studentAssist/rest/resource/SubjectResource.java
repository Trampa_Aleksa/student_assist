package studentAssist.rest.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import studentAssist.repository.RepositoryStorage;
import studentAssist.table.Module;
import studentAssist.table.Subject;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class SubjectResource {

    private int totalNnumberOfSemesters = 8;

    private final
    RepositoryStorage repositoryStorage;

    @Autowired
    public SubjectResource(RepositoryStorage repositoryStorage) {
        this.repositoryStorage = repositoryStorage;
    }

    @GetMapping("/subjects")
    public ResponseEntity<?> getSubjects() {

        List<Subject> subjects = repositoryStorage.getSubjectRepository().findAll();

        return !subjects.isEmpty() ? new ResponseEntity<>(subjects, HttpStatus.OK)
                : new ResponseEntity<>("No subjects available", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/subjects/id")
    public ResponseEntity<?> getSubjectsById(@RequestParam long id) {

        Optional<Subject> subject = repositoryStorage.getSubjectRepository().findById(id);

        return subject.isPresent() ? new ResponseEntity<>(subject.get(), HttpStatus.OK) :
                new ResponseEntity<>("Subject with id: " + id + "Not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("subjects/name")
    public ResponseEntity<?> getSubjectsByName(@RequestParam String name) {

        Subject subject = repositoryStorage.getSubjectRepository().findSubjectByName(name);

        //TODO -- could still have subjects of the same name, need to prevent
        return subject != null ? new ResponseEntity<>(subject, HttpStatus.OK) :
                new ResponseEntity<>("Subject with name: " + name + "Not found", HttpStatus.NOT_FOUND);

    }

    @GetMapping("subjects/semester")
    public ResponseEntity<?> getSubjectsBySemester(@RequestParam int semester) {

        List<Subject> subjects = repositoryStorage.getSubjectRepository().findSubjectsWithSemester(semester);

        //TODO -- single name search instead of list of names?
        return !subjects.isEmpty() ? new ResponseEntity<>(subjects, HttpStatus.OK) :
                new ResponseEntity<>("Subjects with semester: " + semester + "Not found", HttpStatus.NOT_FOUND);

    }

    @GetMapping("subjects/subjectUsers")
    public ResponseEntity<?> getSubjectsOfUser(@RequestParam long id) {

        Optional<Subject> subject = repositoryStorage.getSubjectRepository().findById(id);

        return subject.isPresent()
                ? new ResponseEntity<>(repositoryStorage.getSubjectRepository().findUsersOfSubject(subject.get()), HttpStatus.OK)
                : new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);

    }

    @PostMapping("/subjects")
    public ResponseEntity<?> addSubject(@RequestBody Subject subject) {
        //TODO -- Handling of every invalid subject input
        if ((subject.getSemester() > totalNnumberOfSemesters) || (subject.getSemester() <= 0)) {
            return new ResponseEntity<>("Semester must be between 1 and " + totalNnumberOfSemesters, HttpStatus.PARTIAL_CONTENT);
        }
        if (subject.getName() == null) {
            return new ResponseEntity<>("No name passed to the subject", HttpStatus.PARTIAL_CONTENT);
        }
        Subject savedStudent = repositoryStorage.getSubjectRepository().save(subject);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedStudent.getId()).toUri();

        return ResponseEntity.created(location).build();
    }


    }


