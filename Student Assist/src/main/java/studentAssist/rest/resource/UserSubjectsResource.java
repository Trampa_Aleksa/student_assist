package studentAssist.rest.resource;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import studentAssist.repository.RepositoryStorage;
import studentAssist.table.Subject;
import studentAssist.table.User;
import studentAssist.table.UserSubject;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class UserSubjectsResource {

    private final
    RepositoryStorage repositoryStorage;

    @Autowired
    public UserSubjectsResource(RepositoryStorage repositoryStorage) {
        this.repositoryStorage = repositoryStorage;
    }

    @GetMapping("/userSubjects")
    public ResponseEntity<?> getUserSubjects() {

        List<UserSubject> userSubjects = repositoryStorage.getUserSubjectRepository().findAll();

        return !userSubjects.isEmpty()
                ? new ResponseEntity<>(userSubjects, HttpStatus.OK)
                : new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);

    }

    @JsonIgnoreProperties
    @GetMapping("/userSubjects/id")
    public ResponseEntity<?> getSubjectsOfUser(@RequestParam long id) {

        Optional<User> user = repositoryStorage.getUserRepository().findById(id);
        System.out.println(user);
        return user.isPresent()
                ? new ResponseEntity<>(repositoryStorage.getUserRepository().getSubjectsOfUser(user.get()), HttpStatus.OK)
                : new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);

    }

    @PostMapping("/userSubjects/post/name")
    public ResponseEntity<?> addSubjectToUser(@RequestParam String name, @RequestParam long userId) {

        Subject subject = repositoryStorage.getSubjectRepository().findSubjectByName(name);
        Optional<User> user = repositoryStorage.getUserRepository().findById(userId);

              return handleAddSubjectToUserRequest(subject,user.get());

    }

    @PostMapping("/userSubjects/post/id")
    public ResponseEntity<?> addSubjectToUser(@RequestParam long subjectId, @RequestParam long userId) {

        Optional<Subject> subject = repositoryStorage.getSubjectRepository().findById(subjectId);
        Optional<User> user = repositoryStorage.getUserRepository().findById(userId);

        return handleAddSubjectToUserRequest(subject.get(), user.get());
    }

    /**
     * Adding subjects to user can be called via different params, call this method to prevent duplicate code.
     * @param subject subject to add to user . Can be null - returns response message
     * @param user user to add subject to. Can be null - returns response message
     * @return a response entity with the URI location of the added subject or a message if a param was null
     */
    private ResponseEntity<?> handleAddSubjectToUserRequest( Subject subject, User user){

        if(subject == null){
            return new ResponseEntity<>("Subject was not found. Did you pass in the right parameter?", HttpStatus.BAD_REQUEST);
        }
        if(user == null){
            return new ResponseEntity<>("User was not found. Did you pass in the right parameter?", HttpStatus.BAD_REQUEST);
        }

        UserSubject us = repositoryStorage.getUserSubjectRepository().save(new UserSubject(user, subject));

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(us.getId()).toUri();
        return ResponseEntity.created(location).build();

    }
}
